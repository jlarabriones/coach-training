import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { FileService } from './file.service';
import { LocalStorageService } from 'angular-2-local-storage';

@Injectable()
export class FileResolve implements Resolve<any> {

  constructor(private fileService: FileService, private localStorageService: LocalStorageService) {}
  resolve(route: ActivatedRouteSnapshot) {
     let idFile = route.params['id'];
     return this.fileService.getFileById(idFile);
  }
}
