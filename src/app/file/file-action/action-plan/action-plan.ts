import {Component, OnInit, Input} from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import {RouterModule, Routes, Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'action-plan',
  templateUrl: './action-plan.html'
})
export class ActionPlanComponent implements OnInit{

  @Input() file: any;

  constructor(
    private localStorageService: LocalStorageService,
    private router: Router,
    private route: ActivatedRoute) {
  }

  ngOnInit(){
  }
}
