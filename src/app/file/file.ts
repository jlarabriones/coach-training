import {Component, OnInit} from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import {RouterModule, Routes, Router, ActivatedRoute} from '@angular/router';
import { FileService } from './file.service';

@Component({
  selector: 'file',
  templateUrl: './file.html'
})
export class FileComponent implements OnInit{
  private user: any;
  public file: any;
  public isPlanCreated: boolean;
  public isOpen: boolean;

  constructor(
    private localStorageService: LocalStorageService,
    private router: Router,
    private route: ActivatedRoute,
    private fileService: FileService) {

  }

  ngOnInit(){
    this.file = this.route.snapshot.data['file'];
  }

  createActionPlan() {
    this.isOpen = true;
  }

  closeModalCreateActionPlan(modalOpen: boolean) {
    this.isOpen = modalOpen;
  }
}
