/****** angular imports ******/
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import { HttpModule, JsonpModule, Http, XHRBackend, RequestOptions } from '@angular/http';
import { FormsModule }   from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
/***** components ******/
import {routing, RootComponent} from './routes';
import {CompetenceComponent} from './competence/competence';
import {LoginComponent} from './login/login';
import {NavbarComponent} from './navbar/navbar';
import {TestComponent} from './test/test';
// admin
import {AdminComponent} from './admin/admin';
import {UserComponent} from './admin/user-component/user-component';
import {ViewUserComponent} from './admin/view-user/view-user';
import {UserDetailComponent} from './admin/user-component/user-detail/user-detail';
import {CompetenceListComponent} from './admin/competence-list-component/competence-list-component';
import {CompetenceDetailComponent} from './admin/competence-list-component/competence-detail/competence-detail';

// Users
import {UsersComponent} from './users/users';
import {UserDetailEditComponent} from './users/user-detail-edit/user-detail-edit';
import {UserListComponent} from './users/user-list/user-list';
import {ModalCreateUserComponent} from './users/modal-user/modal-create-user';
import {UsersResolve, CompaniesResolve} from './users/users.resolve';

// file && competence
import {FileComponent} from './file/file';
import {ModalCreateFileComponent} from './competence/competence-routines/modal-file/modal-create-file';
import {CompetenceDetailHeaderomponent} from './competence/competence-detail-header/competence-detail-header';
import {ModalCreateCompetencesComponent} from './admin/competence-list-component/modal-competence/modal-create-competence';
import {CompetenceRoutineComponent} from './competence/competence-routines/competence-routine';
import {FileRoutineComponent} from './competence/competence-routines/file-routine/file-routine';
import {CompetenceResolve, RoutinesResolve, FilesResolve} from './competence/competence.resolve';
import {FileHeaderComponent} from './file/file-header/file-header';
import {FileActionComponent} from './file/file-action/file-action';
import {ActionPlanComponent} from './file/file-action/action-plan/action-plan';
import {ModalActionPlanComponent} from './file/modal-action-plan/modal-action-plan';

// not found
import {NotFoundComponent} from './not-found-404/not-found';

/***** services ******/
import {LoginService} from './login/login.service';
import {FileService} from './file/file.service';
import {AdminService} from './admin/admin.service';
import {UserService} from './users/user.service';
import {CompetenceService} from './competence/competence.service';
import {AuthGuard} from './commons/auth.guards';
import { LocalStorageModule } from 'angular-2-local-storage';
import { ChartModule } from 'angular2-highcharts';
import {httpFactory} from "./shared/http.factory";
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {ModalModule} from "ngx-modal";
import { MyDatePickerModule } from 'mydatepicker';
import { DatePickerModule } from 'ng2-datepicker';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HighchartsStatic } from 'angular2-highcharts/dist/HighchartsService';

/****** resolves ******/
import {AdminUsersResolve} from './admin/admin-users.resolve';
import {FileResolve} from './file/file.resolve';




@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    routing,
    LocalStorageModule.withConfig({
            prefix: 'my-app',
        //  storageType: 'localStorage'
            storageType: 'sessionStorage'
        }),
    ChartModule,
    HttpModule,
    JsonpModule,
    FormsModule,
    NgxChartsModule,
    ModalModule,
    MyDatePickerModule,
    DatePickerModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
   { provide: 'windowObject', useValue: window},
   {
    provide: HighchartsStatic,
    useFactory: highchartsFactory
  },
   {
    provide: Http,
    useFactory: httpFactory,
    deps: [XHRBackend, RequestOptions]
  },
   LoginService,
   AdminService,
   UserService,
   FileService,
   AdminUsersResolve,
   CompaniesResolve,
   CompetenceResolve,
   RoutinesResolve,
   FilesResolve,
   FileResolve,
   UsersResolve,
   FileResolve,
   CompetenceService,
   AuthGuard
 ],
  declarations: [
    RootComponent,
    CompetenceComponent,
    LoginComponent,
    NavbarComponent,
    AdminComponent,
    FileComponent,
    ModalCreateFileComponent,
    ModalCreateCompetencesComponent,
    TestComponent,
    UserComponent,
    UserDetailComponent,
    CompetenceListComponent,
    CompetenceDetailComponent,
    UsersComponent,
    UserDetailEditComponent,
    UserListComponent,
    ModalCreateUserComponent,
    ViewUserComponent,
    CompetenceDetailHeaderomponent,
    CompetenceRoutineComponent,
    FileRoutineComponent,
    FileHeaderComponent,
    FileActionComponent,
    ActionPlanComponent,
    NotFoundComponent,
    ModalActionPlanComponent
  ],
  bootstrap: [RootComponent]
})
export class AppModule {}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

export function highchartsFactory() {
  return require('highcharts');
}
