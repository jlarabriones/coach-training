export function createUser() {
  return {
     pass: '',
     email: '',
     idCoach: '',
     rol:{
       admin: false,
       coach: false,
       user: false
     },
     company: {
      id: '',
      name: ''
    },
     area: '',
     position: '',
     personData: {
      name: '',
      surname: '',
      birthDate: '',
    },
     createdAt: '',
     updatedAt: ''
  }
}
