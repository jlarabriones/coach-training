export class Goal {
    constructor(
        public name: string,
        public description: string,
        public dateStart: Date,
        public dateFinish: Date,
        public dateCreated: Date,
        public notes: string,
        public userId: string
    ) {
    }
}
