import {XHRBackend, Http, RequestOptions} from "@angular/http";
import {InterceptedHttp} from "./http.interceptor";
import { LocalStorageService } from 'angular-2-local-storage';

export function httpFactory(xhrBackend: XHRBackend, requestOptions: RequestOptions, localStorageService: LocalStorageService): Http {
    return new InterceptedHttp(xhrBackend, requestOptions, localStorageService);
}
