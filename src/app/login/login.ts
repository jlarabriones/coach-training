import {Component, OnInit} from '@angular/core';
import {RouterModule, Routes, Router} from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';
import { LoginService } from './login.service'

@Component({
  selector: 'login',
  templateUrl: './login.html'
})
export class LoginComponent implements OnInit{

  public user: Object;
  public userData: any;

  constructor(
    private localStorageService: LocalStorageService,
    private loginService: LoginService,
    private router: Router) {
  }

  ngOnInit(){
  }

  onLogin(form: any, email: any, pass: any){
    this.user = {
      email: email,
      pass: pass
    };
    this.loginService.postUser(this.user)
                   .subscribe(
                      data => {
                        console.log('postUser', data);
                        this.userData = data;
                        localStorage.setItem('user', JSON.stringify(this.userData.user));
                        localStorage.setItem('token', data.token);
                        this.router.navigate(['admin']);
                      },
                     error => console.log('error en login', error));
  }
}
