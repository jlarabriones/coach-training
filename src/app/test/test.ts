import {Component, OnInit} from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';

@Component({
  selector: 'test',
  templateUrl: './test.html'
})
export class TestComponent implements OnInit{

  public user:String;

  constructor(private localStorageService: LocalStorageService) {
  }

  ngOnInit(){
     // called after the constructor and called  after the first ngOnChanges()
     this.user = localStorage.getItem('user');
  }
}
