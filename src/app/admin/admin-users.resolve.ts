import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { AdminService } from './admin.service';
import { LocalStorageService } from 'angular-2-local-storage';

@Injectable()
export class AdminUsersResolve implements Resolve<any> {

  private user: any;
  constructor(
    private localStorageService: LocalStorageService,
    private adminService: AdminService) {}

  resolve(route: ActivatedRouteSnapshot) {
    this.user = JSON.parse(localStorage.getItem('user'));
    if (this.user.rol.user) {
        // return this.adminService.getUsersByCompany(idCompany);
        return this.adminService.getUsersByCoach(this.user.idCoach);
    }
    var idUser = this.user.id;
    return this.adminService.getUsersByCoach(idUser);
  }
}

@Injectable()
export class CompaniesResolve implements Resolve<any> {
  private user: any;
  constructor(
    private localStorageService: LocalStorageService,
    private adminService: AdminService) {}

  resolve(route: ActivatedRouteSnapshot) {
    this.user = JSON.parse(localStorage.getItem('user'));

    if (this.user.rol.coach){
      return this.user.company;
    }
    return this.adminService.getCompanies();
  }
}
