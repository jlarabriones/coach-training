import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {LocalStorageService} from 'angular-2-local-storage';
import {RouterModule, Routes, Router, ActivatedRoute} from '@angular/router';
import {AdminService} from '../admin.service';

@Component({
  selector: 'user-component',
  templateUrl: './user-component.html'
})
export class UserComponent implements OnInit{

  @Input() users: any;
  @Input() isCoach: boolean;
  @Output() user: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private localStorageService: LocalStorageService,
    private adminService: AdminService,
    private route: ActivatedRoute) {
  }

  ngOnInit(){
    // console.log('userssss', this.users);
  }

  showUserData(event) {
    this.user.emit(event);
  }
}
