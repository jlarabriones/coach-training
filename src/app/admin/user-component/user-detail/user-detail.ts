import {Component, OnInit, ViewChild, Input, Output, EventEmitter} from '@angular/core';
import {LocalStorageService} from 'angular-2-local-storage';
import {RouterModule, Routes, Router, ActivatedRoute} from '@angular/router';
import {ModalModule} from "ngx-modal";

@Component({
  selector: 'user-detail',
  templateUrl: './user-detail.html'
})
export class UserDetailComponent implements OnInit{

  @Input() users: any;
  @Input() isCoach: boolean;
  @Output() user: EventEmitter<any> = new EventEmitter<any>();

  public cursorStyle: string;

  constructor() {
  }

  ngOnInit(){
    console.log('userrr detail.ts', this.users);
    if (!this.isCoach) {
      this.cursorStyle = 'auto';
    }
  }

  userSelected(user: any) {
    if (this.isCoach) {
      this.user.emit(user);
    }
  }
}
