import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {LocalStorageService} from 'angular-2-local-storage';

@Component({
  selector: 'view-user',
  templateUrl: './view-user.html'
})
export class ViewUserComponent implements OnInit{

  @Input() user: any;
  @Input() isCoach: boolean;

  constructor() {
  }

  ngOnInit(){
  }
}
