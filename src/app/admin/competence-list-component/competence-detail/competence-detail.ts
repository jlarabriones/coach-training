import {Component, OnInit, Input, Output, EventEmitter, SimpleChanges} from '@angular/core';
import {LocalStorageService} from 'angular-2-local-storage';
import {RouterModule, Routes, Router, ActivatedRoute} from '@angular/router';
import {AdminService} from '../../admin.service';
import {ModalModule} from "ngx-modal";

@Component({
  selector: 'competence-detail',
  templateUrl: './competence-detail.html'
})
export class CompetenceDetailComponent implements OnInit{

  @Input() competence: any;
  @Input() isCoach: boolean;

  constructor(
    private localStorageService: LocalStorageService,
    private adminService: AdminService,
    private router: Router,
    private modalModule: ModalModule) {
  }

  ngOnInit(){
  }

  goToCompetence() {
    this.router.navigate(['competence', this.competence.id]);
  }
}
