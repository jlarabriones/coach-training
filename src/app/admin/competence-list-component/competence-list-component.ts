import {Component, OnInit, Input, Output, EventEmitter, SimpleChanges} from '@angular/core';
import {LocalStorageService} from 'angular-2-local-storage';
import {RouterModule, Routes, Router, ActivatedRoute} from '@angular/router';
import {AdminService} from '../admin.service';
import {ModalModule} from "ngx-modal";

@Component({
  selector: 'competence-list-component',
  templateUrl: './competence-list-component.html'
})
export class CompetenceListComponent implements OnInit{

  @Input() competences: any;
  @Input() user: any;
  @Input() showCompetence: boolean;
  @Input() isCoach: boolean;
  @Output() competence: EventEmitter<any> = new EventEmitter<any>();

  public modal: any;
  public isOpen: boolean;

  constructor(
    private localStorageService: LocalStorageService,
    private adminService: AdminService,
    private router: ActivatedRoute,
    private modalModule: ModalModule) {
  }

  ngOnInit(){

    this.isOpen = false;
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('ven lleno cabron!!!', this.user);
  }

  createCompetence() {
    this.isOpen = true;
  }

  closeModalCreateCompetence(modalOpen: boolean) {
    this.isOpen = modalOpen;
  }

  addNewCompetenceToList(competence) {
    this.competences.push(competence);
  }

  /* ngOnChanges(changes: SimpleChanges) {
      this.showCompetence = changes.currentValue;
  }*/
}
