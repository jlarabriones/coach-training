import {Component, OnInit, ViewChild, SimpleChanges, Input, Output, EventEmitter} from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import {RouterModule, Routes, Router, ActivatedRoute} from '@angular/router';
import {ModalModule} from "ngx-modal";
import {AdminService} from '../../admin.service';
import {IMyDpOptions} from 'mydatepicker';
import { DatePickerOptions, DateModel } from 'ng2-datepicker';
// import { createUser } from '../../models/user.model'

@Component({
  selector: 'modal-create-competence',
  templateUrl: './modal-create-competence.html'
})
export class ModalCreateCompetencesComponent implements OnInit {
  public users: any;
  public modal: any;
  public showFade: boolean;
  public userObject: any;
  public isOpen = false;
  public disabled: boolean;
  public roles: any;
  public newCompetence: any;
  public coach: any;

  // dates
  public startDate: any;
  public finishDate: any;



  calendarOptions = {
     format: "DD/MM/YYYY",
     firstWeekdaySunday: false
   };

  @Input() open: any;
  @Input() user: any;
  // @Input() companies: any;
  @Output() isOpenModal: EventEmitter<any> = new EventEmitter<any>();
  @Output() newCompetenceCreated: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modalFile') modalFile: any;

  constructor(
    private localStorageService: LocalStorageService,
    private router: Router,
    private adminService: AdminService,
    private route: ActivatedRoute,
    private modalModule: ModalModule) {
  }

  ngOnInit() {
    this.showFade = true;
    this.disabled = true;
    this.coach = JSON.parse(localStorage.getItem('user'));
    this.newCompetence = {
      name: '',
      description: '',
      idUser: '',
      idCoach: '',
      startDate: '',
      finishDate: '',
      createdAt: ''
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    for (let open in changes) {
      let chng = changes[open];
      this.isOpen = chng.currentValue;
    }
    if (this.isOpen) {
      this.showFade = true;
      this.modalFile.open();
    }
  }

  closeModalCreateCompetence() {
    this.showFade = false;
    this.isOpen = false;
    this.isOpenModal.emit(this.isOpen);
    this.modalFile.close();
  }
  createCompetence() {
    this.newCompetence.idUser = this.user.id;
    this.newCompetence.idCoach = this.coach.id;
    this.newCompetence.startDate = this.startDate.formatted;
    this.newCompetence.finishDate = this.finishDate.formatted;
    this.adminService.postCreateCompetence(this.user.id, this.newCompetence)
      .subscribe(
      data => {
        console.log('adminService.postCreateCompetence', data);
        this.isOpen = false;
        this.newCompetenceCreated.emit(data);
        this.isOpenModal.emit(this.isOpen);
        this.modalFile.close();
        this.showFade = false;
      },
      error => {
        console.error('adminService.postCreateCompetence', error);
        this.isOpen = false;
        this.showFade = false;
        this.isOpenModal.emit(this.isOpen);
      });
  }

  // Private functions
  _transformData(date) {
    date = new Date(date);
    var month = date.getMonth() + 1; //months from 1-12
    var day = date.getDate();
    var year = date.getFullYear();
    date = { date: { year: year, month: month, day: day } }
    return date;
  }
}
