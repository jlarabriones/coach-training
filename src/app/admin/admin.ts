import {Component, OnInit, ViewChild} from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import {RouterModule, Routes, Router, ActivatedRoute} from '@angular/router';
import {AdminService} from './admin.service';

@Component({
  selector: 'admin',
  templateUrl: './admin.html'
})
export class AdminComponent implements OnInit{

  @ViewChild('myModal')myModal: any;

  public users: any;
  public userSelected: any;
  public companies: any;
  public showCompetence: boolean;
  public competences: any;
  public userLogged: any;
  public isCoach: boolean;

  public userObject: any;
  public showUser: boolean;

  constructor(
    private localStorageService: LocalStorageService,
    private router: Router,
    private adminService: AdminService,
    private route: ActivatedRoute) {
  }

  ngOnInit(){
    console.log("entramos tal que aqui");
    this.showCompetence = false;
    // this.users = this.route.snapshot.data['users'];
    this.userLogged = JSON.parse(localStorage.getItem('user'));
    this.isCoach = true;

    // Comprobamos si el usuario logado es coach o user
    if (this.userLogged.rol.user && !this.userLogged.rol.coach) {
      this.isCoach = false;
      this.showUser = true;
      this.userSelected = this.userLogged;
      this.adminService.getCompetencesByIdUser(this.userLogged.id)
      .subscribe(
        data => {
          this.competences = data;
          this.showCompetence = true;
          console.log('adminService.getCompetencesByIdUser', data);
        },
        error => {
          console.error('adminService.getCompetencesByIdUser', error);
        }
      )
      this.adminService.getUsersByCoach(this.userLogged.idCoach)
      .subscribe(
        data => {
          this.users = data;
          console.log('adminService.getCompetencesByIdUser', data);
        },
        error => {
          console.error('adminService.getCompetencesByIdUser', error);
        }
      )
    } else if (!this.userLogged.rol.user && this.userLogged.rol.coach) {
      this.isCoach = true;
      this.showUser = false;
      this.userSelected = this.userLogged;
      this.adminService.getUsersByCoach(this.userLogged.id)
      .subscribe(
        data => {
          this.users = data;
          console.log('adminService.getCompetencesByIdUser', data);
        },
        error => {
          console.error('adminService.getCompetencesByIdUser', error);
        }
      );
    }
    // this.companies = this.route.snapshot.data['companies'];
  }

  showUserData(data) {
    this.userSelected = data;
    this.adminService.getCompetencesByIdUser(this.userSelected.id)
    .subscribe(
      data => {
        this.competences = data;
        this.showUser = true;
        this.showCompetence = true;
        console.log('adminService.getCompetencesByIdUser', data);
      },
      error => {
        console.error('adminService.getCompetencesByIdUser', error);
      }
    )
  }
}
