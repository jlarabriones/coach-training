import {Component, OnInit, Input} from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import {RouterModule, Routes, Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'competence-detail-header',
  templateUrl: './competence-detail-header.html'
})
export class CompetenceDetailHeaderomponent implements OnInit{

  @Input() competence: any;

  constructor(){}

  ngOnInit() {
    console.log(this.competence);
  }

}
