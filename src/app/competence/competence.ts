import {Component, OnInit} from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import {RouterModule, Routes, Router, ActivatedRoute} from '@angular/router';
import {single} from './data';
import {ModalModule} from "ngx-modal";
import {CompetenceService} from './competence.service'

@Component({
  selector: 'competence',
  templateUrl: './competence.html'
})
export class CompetenceComponent implements OnInit{

  public userLogged:any;
  public isCoach: boolean;
  public goal: any;
  public isPlanCreated: boolean;
  public hideFiles: boolean;
  public modal: any;
  public isOpen: boolean;
  public competence: any;
  public routines: any;
  public files: any;
  public routinesToSend: any;

  private sub: any;

  single: any[];
  view: any[] = [700, 400];
  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Name of the competence';
  showYAxisLabel = true;
  yAxisLabel = 'Hours';

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA', '#D1A00C']
  };

  constructor(
    private localStorageService: LocalStorageService,
    private router: Router,
    private route: ActivatedRoute,
    private modalModule: ModalModule,
    private competenceService: CompetenceService)
    {
      Object.assign(this, {single});
    }

  ngOnInit() {
    this.competence = this.route.snapshot.data['competence'];
    this.routines = this.route.snapshot.data['routines'];
    this.files = this.route.snapshot.data['files'];
    this.parseRoutiniesAndFiles(this.routines, this.files);
    this.isOpen = false;
    this.userLogged = JSON.parse(localStorage.getItem('user'));
    this.isCoach = false;
    if (this.userLogged.rol.coach) {
      this.isCoach = true;
    }

  }

  parseRoutiniesAndFiles(routines: any, files: any) {
    for (let i = 0; i < this.routines.length; i++) {
      this.routines[i].assigned = 0;
      this.routines[i].files = [];
      for (let a = 0; a < this.files.length; a++) {
        if (this.routines[i].id === this.files[a].idRoutine) {
          this.routines[i].assigned = this.routines[i].assigned + 1;
          this.routines[i].files.push(this.files[a]);
        }
      }
    }
  }

  createFile() {
    this.isOpen = true;
  }

  closeModalCreateFile(modalOpen: boolean) {
    this.isOpen = modalOpen;
  }

}
