import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class CompetenceService {
  constructor (private http: Http) {
  }

  getCompetenceById(idCompetence) {
    return this.http.get('/competence/' + idCompetence)
                   .map(this.extractData)
                   .catch(this.handleError);
  }

  getAllRoutines() {
    return this.http.get('/routines')
                   .map(this.extractData)
                   .catch(this.handleError);
  }

  getRoutineById(idRoutine) {
    return this.http.get('/routines/' + idRoutine)
                   .map(this.extractData)
                   .catch(this.handleError);
  }

  postCreateFile(idRoutine, file) {
    return this.http.post('/file/routine/' + idRoutine + '/create', file)
                   .map(this.extractData)
                   .catch(this.handleError);
  }

  getAllFilesByCompetenceAndUser(idCompetence, idUser) {
    return this.http.get('/competence/' + idCompetence + '/files/' + idUser)
                   .map(this.extractData)
                   .catch(this.handleError);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body;
  }

  private handleError (error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
