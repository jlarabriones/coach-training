import {Component, OnInit, Input} from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import {RouterModule, Routes, Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'file-routine',
  templateUrl: './file-routine.html'
})
export class FileRoutineComponent implements OnInit{

  @Input() file: any;

  public language: string;

  constructor(
    private localStorageService: LocalStorageService,
    private router: Router){}

  ngOnInit() {
    console.log('fileeeeeeee', this.file);
    this.language = localStorage.language.toUpperCase();
  }

  goToFile() {
    this.router.navigate(['file', this.file.id]);
  }
}
