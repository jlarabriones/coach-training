import {Component, OnInit, ViewChild, SimpleChanges, Input, Output, EventEmitter} from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import {RouterModule, Routes, Router, ActivatedRoute} from '@angular/router';
import {ModalModule} from "ngx-modal";
import {CompetenceService} from '../../competence.service';
import {IMyDpOptions} from 'mydatepicker';
import { DatePickerOptions, DateModel } from 'ng2-datepicker';

@Component({
  selector: 'modal-create-file',
  templateUrl: './modal-create-file.html'
})
export class ModalCreateFileComponent implements OnInit {
  public user: any;
  public modal: any;
  public showFade: boolean;
  public isOpen = false;
  public disabled: boolean;
  public roles: any;
  public file: any;
  public dateStart: any;
  public dateFinish: any;
  // private date: any;
  calendarOptions = {
     format: "DD/MM/YYYY",
     firstWeekdaySunday: false
   };

  @Input() open: any;
  @Input() routine: any;
  @Input() competence: any;
  // @Input() companies: any;
  @Output() isOpenModal: EventEmitter<any> = new EventEmitter<any>();
  @Output() newFile: EventEmitter<any> = new EventEmitter<any>();
  // @Output() newUser: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modalFile') modalFile: any;

  constructor(
    private localStorageService: LocalStorageService,
    private router: Router,
    private competenceService: CompetenceService,
    private route: ActivatedRoute,
    private modalModule: ModalModule) {

  }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.showFade = true;
    this.disabled = true;
    this.file = {
      name: '',
      description: '',
      state: '',
      startDate: '',
      finishDate: '',
      idCompetence: '',
      idUser: '',
      idCoach: '',
      idRoutine: '',
      createdAt: ''
    };
  }

  ngOnChanges(changes: SimpleChanges) {
    for (let open in changes) {
      let chng = changes[open];
      this.isOpen = chng.currentValue;
    }
    if (this.isOpen) {
      this.showFade = true;
      this.modalFile.open();
    }
  }

  closeModal() {
    this.showFade = false;
    this.isOpen = false;
    this.isOpenModal.emit(this.isOpen);
    this.modalFile.close();
  }

  createNewFile() {
    this.file.startDate = this.dateStart.formatted;
    this.file.finishDate = this.dateFinish.formatted;
    this.file.idUser = this.user.id;
    this.file.idCoach = this.user.idCoach;
    this.file.idRoutine = this.routine.id;
    this.file.idCompetence = this.competence.id;
    this.competenceService.postCreateFile(this.routine.id, this.file).subscribe(
      data => {
        console.log('competenceService.postCreateFile', data);
        this.newFile.emit(data);
        this.closeModal();
      },
      error => {
        console.error('error competenceService.postCreateFile', error);
        this.closeModal();
      }
    );
  }
}
