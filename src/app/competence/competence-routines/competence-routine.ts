import {Component, DoCheck, OnInit, Input} from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import {RouterModule, Routes, Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'competence-routine',
  templateUrl: './competence-routine.html'
})
export class CompetenceRoutineComponent implements OnInit, DoCheck{

  @Input() routine: any;
  @Input() isCoach: boolean;
  @Input() competence: any;

  public language: string;
  public isOpen: boolean;
  public files: any;

  constructor(private localStorageService: LocalStorageService){}

  ngOnInit() {
    this.isOpen = false;
  }

  ngDoCheck() {
    let lg = localStorage.getItem('language');
    this.language = lg.toUpperCase();
  }

  createFile() {
    this.isOpen = true;
  }
  closeModalCreateFile(modalOpen: boolean) {
    this.isOpen = modalOpen;
  }
  addNewFileToArray(newFile: any) {
    this.routine.files.push(newFile);
    this.routine.assigned = this.routine.files.length; 
  }
  
}
