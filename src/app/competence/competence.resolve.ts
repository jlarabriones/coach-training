import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CompetenceService } from './competence.service';
import { LocalStorageService } from 'angular-2-local-storage';

@Injectable()
export class CompetenceResolve implements Resolve<any> {
  constructor(private competenceService: CompetenceService) {}

  resolve(route: ActivatedRouteSnapshot) {
     let idCompetence = route.params['id'];
     return this.competenceService.getCompetenceById(idCompetence);
  }
}

@Injectable()
export class RoutinesResolve implements Resolve<any> {
  constructor(private competenceService: CompetenceService) {}

  resolve(route: ActivatedRouteSnapshot) {
     return this.competenceService.getAllRoutines();
  }
}

@Injectable()
export class FilesResolve implements Resolve<any> {
  public userLogged: any;
  constructor(private competenceService: CompetenceService, private localStorageService: LocalStorageService) {}

  resolve(route: ActivatedRouteSnapshot) {
    this.userLogged = JSON.parse(localStorage.getItem('user'));
    let idCompetence = route.params['id'];
    return this.competenceService.getAllFilesByCompetenceAndUser(idCompetence, this.userLogged.id);
  }
}
