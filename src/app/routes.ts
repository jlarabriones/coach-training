import {Component, OnDestroy} from '@angular/core';
import {RouterModule, Routes, Router} from '@angular/router';
// components
import {CompetenceComponent} from './competence/competence';
import {CompetenceResolve, RoutinesResolve, FilesResolve} from './competence/competence.resolve';
import {LoginComponent} from './login/login';
import {NavbarComponent} from './navbar/navbar';
import {AdminComponent} from './admin/admin';
import {AdminUsersResolve} from './admin/admin-users.resolve';
import {FileComponent} from './file/file';
import {TestComponent} from './test/test';
import {FileResolve} from './file/file.resolve';
import {UsersComponent} from './users/users';
import {UsersResolve, CompaniesResolve} from './users/users.resolve';
import {NotFoundComponent} from './not-found-404/not-found';

// services
import {LoginService} from './login/login.service';
import { AuthGuard } from './commons/auth.guards';

import { LocalStorageService } from 'angular-2-local-storage';

@Component({
  selector: 'fountain-root',
  template: `
      <router-outlet>
      </router-outlet>
    `,
})
export class RootComponent {
  public isLogged: boolean;
  public user: Object;
  constructor(
    private localStorageService: LocalStorageService,
    private router: Router,
    private loginService: LoginService
    ) {
  }
}

export const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'competence/:id',
    component: CompetenceComponent,
    resolve: {
     competence: CompetenceResolve,
     routines: RoutinesResolve,
     files: FilesResolve
   },
    canActivate: [AuthGuard]
  },
  {
    path: 'file/:id',
    component: FileComponent,
    canActivate: [AuthGuard],
    resolve: {
      file: FileResolve
    }
  },
  {
    path: 'admin',
    component: AdminComponent,
    resolve: {
     users: UsersResolve
     // companies: CompaniesResolve
   },
    canActivate: [AuthGuard]
  },
  {
    path: 'users',
    component: UsersComponent,
    resolve: {
     companies: CompaniesResolve,
     users: UsersResolve
   },
    canActivate: [AuthGuard]
  },
  {
    path: 'test',
    component: TestComponent,
    resolve: {
     users: AdminUsersResolve
   },
    canActivate: [AuthGuard]
  },
  {
    path: '**',
    component: AdminComponent,
    canActivate: [AuthGuard]
  }
];

export const routing = RouterModule.forRoot(routes);
