import {Component, OnInit, ViewChild, SimpleChanges, Input, Output, EventEmitter} from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import {RouterModule, Routes, Router, ActivatedRoute} from '@angular/router';
import {ModalModule} from "ngx-modal";
import {UserService} from '../user.service';
import {IMyDpOptions} from 'mydatepicker';
import { DatePickerOptions, DateModel } from 'ng2-datepicker';
// import { createUser } from '../../models/user.model'

@Component({
  selector: 'modal-create-user',
  templateUrl: './modal-create-user.html'
})
export class ModalCreateUserComponent implements OnInit {
  public users: any;
  public modal: any;
  public showFade: boolean;
  public userObject: any;
  public isOpen = false;
  public disabled: boolean;
  public roles: any;
  public user: any;
  public companies: any;
  public coach: any;
  private date: any;

  calendarOptions = {
     format: "DD-MM-YYYY",
     firstWeekdaySunday: false
   };

  @Input() open: any;
  // @Input() companies: any;
  @Output() isOpenModal: EventEmitter<any> = new EventEmitter<any>();
  @Output() newUser: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modalUser') modalUser: any;

  constructor(
    private localStorageService: LocalStorageService,
    private router: Router,
    private userService: UserService,
    private route: ActivatedRoute,
    private modalModule: ModalModule) {
  }

  ngOnInit() {
    this.coach = JSON.parse(localStorage.getItem('user'));
    this.userService.getCompanies().subscribe(
    data => {
      console.log('userService.getCompanies', data);
      this.companies = data;
    },
    error => {
      console.error('userService.getCompanies', error);
    });;
    this.showFade = true;
    this.disabled = true;
    this.user = {
      pass: '',
      email: '',
      idCoach: this.coach.id,
      rol:{
        admin: false,
        coach: false,
        user: true
      },
      company: {
       id: '',
       name: ''
     },
      department: '',
      position: '',
      personData: {
       name: '',
       birthDate: '',
     },
      createdAt: '',
      updatedAt: ''
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    for (let open in changes) {
      let chng = changes[open];
      this.isOpen = chng.currentValue;
    }
    if (this.isOpen) {
      this.showFade = true;
      this.modalUser.open();
    }
  }

  closeModalCreateUser() {
    this.showFade = false;
    this.isOpen = false;
    this.isOpenModal.emit(this.isOpen);
    this.modalUser.close();
  }

  createUser() {
    this.user.personData.birthDate = this.date.formatted;
    this.userService.postCreateUser(this.user)
      .subscribe(
      data => {
        console.log('userService.postCreateUser', data);
        this.isOpen = false;
        this.isOpenModal.emit(this.isOpen);
        this.newUser.emit(data);
        this.modalUser.close();
        this.showFade = false;
      },
      error => {
        console.error('userService.postCreateUser', error);
        this.isOpen = false;
        this.showFade = false;
        this.isOpenModal.emit(this.isOpen);
      });
  }
  // Private functions

  _transformData(date) {
    date = new Date(date);
    var month = date.getMonth() + 1; //months from 1-12
    var day = date.getDate();
    var year = date.getFullYear();
    date = { date: { year: year, month: month, day: day } }
    return date;
  }
}
