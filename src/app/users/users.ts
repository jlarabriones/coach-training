import {Component, OnInit, ViewChild} from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import {RouterModule, Routes, Router, ActivatedRoute} from '@angular/router';
import {ModalModule} from "ngx-modal";

@Component({
  selector: 'users',
  templateUrl: './users.html'
})
export class UsersComponent implements OnInit{

  public users: any;
  public modal: any;
  public isOpen: boolean;
  public companies: any;

  constructor(
    private localStorageService: LocalStorageService,
    private router: Router,
    private route: ActivatedRoute,
    private modalModule: ModalModule) {
  }

  ngOnInit(){
    this.isOpen = false;
    this.users = this.route.snapshot.data['users'];
    this.companies = this.route.snapshot.data['companies'];
    console.log(this.route.snapshot.data);
  }

  createNewUser(user: any) {
    this.users.push(user);
  }

  createUser() {
    this.isOpen = true;
  }

  closeModalCreateUser(modalOpen: boolean) {
    this.isOpen = modalOpen;
  }

  deleteUser(user) {
    let index = this.users.indexOf(user);
    for (let i = 0; i < this.users.length; i++) {
      if (this.users[i].id === user.id) {
          this.users.splice(i, 1);
      }
    }
  }
}
