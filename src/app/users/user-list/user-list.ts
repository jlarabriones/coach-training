import {Component, OnInit, Input, Output, EventEmitter, SimpleChanges} from '@angular/core';
import {LocalStorageService} from 'angular-2-local-storage';
import {RouterModule, Routes, Router, ActivatedRoute} from '@angular/router';
import {UserService} from '../user.service';

@Component({
  selector: 'user-list',
  templateUrl: './user-list.html'
})
export class UserListComponent implements OnInit{

  @Input() user: any;
  @Input() companies: any;
  @Output() userToDelete: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private localStorageService: LocalStorageService,
    private userService: UserService,
    private router: Router) {
  }

  ngOnInit(){
  }

  deleteUser() {
    this.userService.deleteUser(this.user.id)
      .subscribe(
      data => {
        console.log('updateUserById.deleteUser', data);
        this.userToDelete.emit(data);
      },
      error => {
        console.error('userService.deleteUser', error);
      });
  }
}
