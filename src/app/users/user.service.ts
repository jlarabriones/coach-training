import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map'

@Injectable()
export class UserService {
  constructor (private http: Http) {
  }

  getUsersByCoach(userId) {
    return this.http.get('/users/coach/' + userId)
                   .map(this.extractData)
                   .catch(this.handleError);
  }

  getUsersByCompany(companyId) {
    return this.http.get('/users/company/' + companyId)
                   .map(this.extractData)
                   .catch(this.handleError);
  }

  updateUserById(userId, data) {
    return this.http.put('/user/' + userId + '/update', data)
                   .map(this.extractData)
                   .catch(this.handleError);
  }

  postCreateUser(data) {
    return this.http.post('/users/create', data)
                   .map(this.extractData)
                   .catch(this.handleError);
  }

  deleteUser(userId) {
    return this.http.delete('/users/' + userId + '/delete')
                   .map(this.extractData)
                   .catch(this.handleError);
  }

  getCompanies() {
    return this.http.get('/companies')
                   .map(this.extractData)
                   .catch(this.handleError);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body;
  }

  private handleError (error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
