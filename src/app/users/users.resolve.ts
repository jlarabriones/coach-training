import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { UserService } from './user.service';
import { LocalStorageService } from 'angular-2-local-storage';

@Injectable()
export class UsersResolve implements Resolve<any> {
  private user: any;
  constructor(
    private localStorageService: LocalStorageService,
    private userService: UserService) {}

  resolve(route: ActivatedRouteSnapshot) {
    this.user = JSON.parse(localStorage.getItem('user'));
    if (this.user.rol.user) {
        // return this.adminService.getUsersByCompany(idCompany);
        return this.userService.getUsersByCoach(this.user.idCoach);
    }
    var idUser = this.user.id;
    return this.userService.getUsersByCoach(idUser);
  }
}

@Injectable()
export class CompaniesResolve implements Resolve<any> {
  private user: any;
  constructor(
    private localStorageService: LocalStorageService,
    private userService: UserService) {
    }


  resolve(route: ActivatedRouteSnapshot) {
    this.user = JSON.parse(localStorage.getItem('user'));
    return this.userService.getCompanies();
  }
}
