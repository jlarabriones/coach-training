import {Component, OnInit, Input, Output, EventEmitter, SimpleChanges} from '@angular/core';
import {LocalStorageService} from 'angular-2-local-storage';
import {RouterModule, Routes, Router, ActivatedRoute} from '@angular/router';
import {ModalModule} from "ngx-modal";
import {UserService} from '../user.service';

@Component({
  selector: 'user-detail-edit',
  templateUrl: './user-detail-edit.html'
})
export class UserDetailEditComponent implements OnInit{

  @Input() showUser: any;
  @Input() user: any;
  @Input() companies: any;

  public userToChange: any;

  constructor(
    private localStorageService: LocalStorageService,
    private router: Router,
    private userService: UserService,
    private modalModule: ModalModule) {
  }

  ngOnInit(){
    this.userToChange = Object.assign({}, this.user);
  }

  updateUser() {
    this.userService.updateUserById(this.user.id, this.userToChange)
      .subscribe(
      data => {
        console.log('updateUserById.postCreateUser', data);
      },
      error => {
        console.error('userService.postCreateUser', error);
      });
  }

  preSelectedOption(option: any) {
    // if (option && (option.id === this.userToChange.company.id)) {
    //   return true;
    // }
    return false;
  }

}
