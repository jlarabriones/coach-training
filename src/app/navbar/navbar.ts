import {Component, OnInit} from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import {RouterModule, Routes, Router} from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'navbar',
  templateUrl: './navbar.html'
})
export class NavbarComponent implements OnInit{

  public user:String;
  options: Object;
  public language: string;
  public userLogged: any;

  constructor(
    private localStorageService: LocalStorageService,
    private router: Router,
    private translate: TranslateService) {
      translate.setDefaultLang('en');
      this.language = 'en';
      localStorage.setItem('language', this.language);
      this.userLogged = JSON.parse(localStorage.getItem('user'));
  }
  switchLanguage(language: string) {
    this.translate.use(language);
    this.language = language;
    localStorage.setItem('language', this.language);
  }

  ngOnInit(){

  }

  onLogout() {
    localStorage.removeItem('user');
    localStorage.removeItem('token');
    this.router.navigate(['login']);
  }
}
