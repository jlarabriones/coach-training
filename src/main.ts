// import 'core-js/client/shim';
// import 'zone.js/dist/zone';

import '@angular/common';
import 'rxjs';

// import './assets/css/main.scss';

import {enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {AppModule} from './app/app.module';
declare var require: any;
const conf = require('./app/config.json');

declare var process: any;

if (process.env.NODE_ENV === 'production') {
  conf.environment = 'production';
  enableProdMode();
} else {
  conf.environment = 'local';
}
// else {
//   Error['stackTraceLimit'] = Infinity; // tslint:disable-line:no-string-literal
//   require('zone.js/dist/long-stack-trace-zone'); // tslint:disable-line:no-var-requires
// }
platformBrowserDynamic().bootstrapModule(AppModule);
