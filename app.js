const express = require('express');
const cors = require('cors')
const http = require('http');
const path = require('path');
const bluebird = require('bluebird');

const bodyParser = require('body-parser');
// colections
const user = require('./server/user/user');
const competence = require('./server/competence/competence');
const companies = require('./server/companies/companies');
const routines = require('./server/routines/routines');
const file = require('./server/file/file');

const config = require('./server/config');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const router = express.Router();

const app = express();
const port = process.env.PORT || '4000';

// app.use(cors());

// Add headers
app.use(function(req, res, next) {
  console.log('entramos al token: ' + new Date());
  // intercept OPTIONS method authorization and token check
  // if (req.method !== 'OPTIONS' && req.path !== '/api/login' && req.path !== '/') {
  //   if (!req.headers.authorization) {
  //     return res.status(403).send({
  //       message: 'Tu petición no tiene cabecera de autorización'
  //     });
  //   }
  //   const token = req.headers.authorization;
  //   // verify a token symmetric
  //   try {
  //     const decoded = jwt.verify(token, config.secret);
  //     console.log('the token hasnt expired');
  //   } catch (err) {
  //     // err
  //     let errorMessage = '';
  //     if (err.name === 'TokenExpiredError') {
  //       errorMessage = 'Token has expired';
  //     } else {
  //       errorMessage = 'Token error';
  //     }
  //     return res.status(401).send({
  //       message: errorMessage
  //     });
  //   }
  // }

  // Website you wish to allow to connect
  res.header('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.header('Access-Control-Allow-Headers', 'Access-Control-Allow-Methods, Content-Type, Accept, X-Requested-With, remember-me, Authorization');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.header('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});


// Body Parser

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));

app.disable('etag');

app.use('/api', user);
app.use('/api', competence);
app.use('/api', companies);
app.use('/api', routines);
app.use('/api', file);

if (port === '4000') {
  app.use(express.static(path.join(__dirname, 'src')));

  app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'src/index.html'));
  });
} else {
  app.use(express.static(path.join(__dirname, 'dist')));

  app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
  });
}

app.set('port', port);

const server = http.createServer(app);
server.listen(port, () => {
  console.log('running server on port: ' + port);
  mongoose.Promise = bluebird;
  // mongoose.connect('mongodb://localhost:27017/gymbrain');
  mongoose.connect('mongodb://maldini:maldini@ds155811.mlab.com:55811/routines');
  // mongoose.connect('mongodb://jorge:jorge@ds119675.mlab.com:19675/braintrained');

});



// router.get('/', function (req, res, next) {
//   res.sendfile('../src/index.html');
// });
