// grab the things we need
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// create a schema
const proficiencySchema = new Schema({
  name: String,
  description: String,
  dataSheet: [{
    id: String,
    name: String,
    description: String,
    date: String,
    routine: String,
    actionPlan: [{
      id: String,
      name: String,
      revision: Boolean
    }]
  }]
});

// the schema is useless so far
// we need to create a model using it
const Proficiency = mongoose.model('Proficiency', proficiencySchema);

// make this available to our users in our Node applications
module.exports = Proficiency;
