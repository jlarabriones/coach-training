// grab the things we need
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// create a schema
const userSchema = new Schema({
  pass: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  company: {
    id: String,
    name: String
  },
  area: String,
  position: String,
  idCoach: String,
  rol: {
    admin: Boolean,
    coach: Boolean,
    user: Boolean
  },
  createdAt: Date,
  updatedAt: Date,
  personData: {
    name: String,
    surname: String,
    birthDate: String
  }
});

// the schema is useless so far
// we need to create a model using it
const User = mongoose.model('User', userSchema);

// make this available to our users in our Node applications
module.exports = User;
