const express = require('express');
const mongojs = require('mongojs');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const router = express.Router();
const config = require('../config');
// const db = mongojs('mongodb://jorge:jorge@ds155811.mlab.com:55811/routines', ['user']);
const User = require('./user.schema');
const userDto = require('./user.dto');

function createToken(user) {
  if (user !== undefined) {
    return jwt.sign({data: user}, config.secret, {
      expiresIn: '60m'
    });
  }
}
// Get all the users by company
router.get('/users/company/:id', function(req, res, next) {
  User.find({
    "company.id":  mongojs.ObjectId(req.params.id)
  }, function(err, users) {
    if (err) {
      return res.send(err);
    }
    const usersToSend = userDto.serviceListToDto(users);
    res.statusCode = 200;
    return res.json(usersToSend);
  });
});
// Get all the users by coach
router.get('/users/coach/:id', function(req, res, next) {
  console.log('entramos a ver los coah');
  User.find({
    idCoach: mongojs.ObjectId(req.params.id)
  }, function(err, users) {
    if (err) {
      return res.send(err);
    }
    const usersToSend = userDto.serviceListToDto(users);
    res.statusCode = 200;
    return res.json(usersToSend);
  });
});
// get login user
router.post('/login', function(req, res, next) {
  var users = req.body;
  console.log('entramossssssss al de user.js', req.body);
  if (!users.pass || !users.email) {
    res.status(400);
    return res.json({
      status: 400,
      error: 'Bad data'
    });
  }
  User.findOne({
    email: users.email,
    pass: users.pass
  }, function(err, userFound) {
    if (err || userFound === null) {
      res.status(404);
      return res.json({
        status: 404,
        error: 'User not found'
      });
    } else {
      const userToSend = userDto.serviceToDto(userFound);
      return res.json({
        user: userToSend,
        token: createToken(userFound)
      });
    }
  });
});

// Create  user
router.post('/users/create', function(req, res, next) {
  var userToSave = req.body;
  userToSave.pass = Math.random().toString(36).slice(-8);
  userToSave.createdAt = new Date();
  if (!userToSave.email) {
    res.status(400);
    return res.json({
      status: 400,
      error: 'Bad data'
    });
  } else {
    const user = new User(userToSave);
    console.log(user);
    user.save(function(err, userSaved) {
      console.log(err);
      if (err || userSaved === null) {
        res.status(500);
        return res.json({
          status: 404,
          error: 'Error saving the user'
        });
      }
      const userToSend = userDto.serviceToDto(userSaved);
      return res.json(userToSend);
    });
  }
});

// Delete one user by ID
router.delete('/users/:id/delete', function(req, res, next) {
  User.findOneAndRemove({
    _id: mongojs.ObjectId(req.params.id)
  }, function(err, user) {
    if (err) {
      return res.send(err);
    }
    const userToDelete = userDto.serviceToDto(user);
    return res.json(userToDelete);
  });
});

// Update user
router.put('/user/:id/update', function(req, res, next) {
  var user = req.body;
  User.update({
    _id: mongojs.ObjectId(req.params.id)
  }, user, {}, function(err, userUpdated) {
    if (err) {
      return res.send(err);
    }
    const userToUpdate = userDto.serviceToDto(user);
    return res.json(userToUpdate);
  });
});

module.exports = router;
