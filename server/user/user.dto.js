const userDto = {
  admin: false,
  idCoach: '',
  rol: '',
  company: [],
  area: '',
  position: '',
  email: '',
  personData: {
    name: '',
    surname: '',
    birthDate: ''
  },
  createdAt: ''
};

function serviceToDto(serviceData) {
  userDto.id = serviceData._id;
  userDto.idCoach = serviceData.idCoach;
  userDto.admin = serviceData.admin;
  userDto.rol = serviceData.rol;
  userDto.email = serviceData.email;
  for (let i = 0; i < serviceData.company.length; i++) {
    var company = {id: serviceData.company[i]._id, name: serviceData.company[i].name};
    userDto.company.push(company);
  }
  userDto.area = serviceData.area;
  userDto.position = serviceData.position;
  userDto.personData.name = serviceData.personData.name;
  userDto.personData.surname = serviceData.personData.surname;
  userDto.personData.birthDate = serviceData.personData.birthDate;
  userDto.createdAt = serviceData.createdAt;

  return userDto;
}

function serviceListToDto(serviceDataList) {
  const userDtoList = [];
  for (let i = 0; i < serviceDataList.length; i++) {
    const userDto = {
      admin: false,
      idCoach: '',
      rol: '',
      company: {
        id: '',
        name: ''
      },
      area: '',
      position: '',
      email: '',
      personData: {
        name: '',
        surname: '',
        birthDate: ''
      },
      createdAt: ''
    };

    userDto.id = serviceDataList[i]._id;
    userDto.idCoach = serviceDataList[i].idCoach;
    userDto.admin = serviceDataList[i].admin;
    userDto.rol = serviceDataList[i].rol;
    userDto.company.id  = serviceDataList[i].company.id;
    userDto.company.name = serviceDataList[i].company.name;
    userDto.area = serviceDataList[i].area;
    userDto.position = serviceDataList[i].position;
    userDto.email = serviceDataList[i].email;
    userDto.personData.name = serviceDataList[i].personData.name;
    userDto.personData.surname = serviceDataList[i].personData.surname;
    userDto.personData.birthDate = serviceDataList[i].personData.birthDate;
    userDto.createdAt = serviceDataList[i].createdAt;
    userDtoList.push(userDto);
  }

  return userDtoList;
}

exports.serviceToDto = serviceToDto;
exports.serviceListToDto = serviceListToDto;

// module.exports = serviceToDto;
