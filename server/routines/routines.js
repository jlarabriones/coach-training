const express = require('express');
const mongojs = require('mongojs');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const router = express.Router();
const config = require('../config');
// const db = mongojs('mongodb://jorge:jorge@ds155811.mlab.com:55811/routines', ['user']);
const Routine = require('./routines.schema');
const routineDto = require('./routines.dto');

// Get all the routines
router.get('/routines', function(req, res, next) {
  console.log('entramos en routines');
  Routine.find({}, function(err, routines) {
    if (err) {
      return res.send(err);
    }
    // console.log('routines', routines);
    const routinesToSend = routineDto.serviceListToDto(routines);
    res.statusCode = 200;
    return res.json(routinesToSend);
  });
});

// Get all the routines by id
router.get('/routines/:id', function(req, res, next) {
  Competence.find({
    "id":  mongojs.ObjectId(req.params.id)
  }, function(err, routine) {
    if (err) {
      return res.send(err);
    }
    const routineToSend = routineDto.serviceToDto(routine);
    res.statusCode = 200;
    return res.json(routineToSend);
  });
});


module.exports = router;
