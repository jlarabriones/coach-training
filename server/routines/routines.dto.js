function serviceToDto(serviceData) {
  const routineDto = {
    id: '',
    name: {
      ES: '',
      EN:''
    },
    description: {
      ES: '',
      EN:''
    }
  };

  routineDto.id = serviceData._id;
  routineDto.name.ES = serviceData.name.ES;
  routineDto.name.EN = serviceData.name.EN;
  routineDto.description.ES = serviceData.description.ES;
  routineDto.description.EN = serviceData.description.EN;

  return routineDto;
}


function serviceListToDto(serviceDataList) {
  const routineDtoList = [];
  for (let i = 0; i < serviceDataList.length; i++) {
    const routineDto = {
      id: '',
      name: {
        ES: '',
        EN:''
      },
      description: {
        ES: '',
        EN:''
      }
    };

    routineDto.id = serviceDataList[i]._id;
    routineDto.name.ES = serviceDataList[i].name.ES;
    routineDto.name.EN = serviceDataList[i].name.EN;
    routineDto.description.ES = serviceDataList[i].description.ES;
    routineDto.description.EN = serviceDataList[i].description.EN;

    routineDtoList.push(routineDto);
  }

  return routineDtoList;
}

exports.serviceListToDto = serviceListToDto;
exports.serviceToDto = serviceToDto;

// module.exports = serviceToDto;
