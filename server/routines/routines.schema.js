// grab the things we need
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// create a schema
const routineSchema = new Schema({
  name: {
    ES: String,
    EN: String
  },
  description: {
    ES: String,
    EN: String
  }
});

// the schema is useless so far
// we need to create a model using it
const Routine = mongoose.model('Routine', routineSchema);

// make this available to our users in our Node applications
module.exports = Routine;
