const express = require('express');
const router = express.Router();
const Companies = require('./companies.schema');
const companiesDto = require('./companies.dto');

// Get all the users
router.get('/companies', function (req, res, next) {
  Companies.find(function (err, companies) {
    if (err) {
      return res.send(err);
    }
    const companiesToSend = companiesDto.serviceListToDto(companies);
    res.statusCode = 200;
    console.log('llamamos a las compañias', companiesToSend);
    return res.json(companiesToSend);
  });
});

module.exports = router;
