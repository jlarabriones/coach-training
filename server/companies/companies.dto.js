
function serviceListToDto(serviceDataList) {
  const companiesDtoList = [];
  for (let i = 0; i < serviceDataList.length; i++) {
    const companiesDto = {
      id: '',
      name: ''
    };
    companiesDto.id = serviceDataList[i]._id;
    companiesDto.name = serviceDataList[i].name;
    companiesDtoList.push(companiesDto);
  }

  return companiesDtoList;
}

exports.serviceListToDto = serviceListToDto;
