// grab the things we need
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// create a schema
const companiesSchema = new Schema({
  id: {type: String, required: true},
  name: String
});

// the schema is useless so far
// we need to create a model using it
const Companies = mongoose.model('Companies', companiesSchema);

// make this available to our users in our Node applications
module.exports = Companies;
