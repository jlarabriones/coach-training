const express = require('express');
const mongojs = require('mongojs');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const router = express.Router();
const config = require('../config');
// const db = mongojs('mongodb://jorge:jorge@ds155811.mlab.com:55811/routines', ['user']);
const Competence = require('./competence.schema');
const competenceDto = require('./competence.dto');

// Get all the competences by user
router.get('/competence/user/:id', function(req, res, next) {
  Competence.find({
    "idUser":  mongojs.ObjectId(req.params.id)
  }, function(err, competences) {
    if (err) {
      return res.send(err);
    }
    const competenceToSend = competenceDto.serviceListToDto(competences);
    res.statusCode = 200;
    return res.json(competenceToSend);
  });
});

// Create  new competence
router.post('/user/:id/create', function(req, res, next) {
  var competenceToSave = req.body;
  competenceToSave.createdAt = new Date();
  if (!competenceToSave.name) {
    res.status(400);
    return res.json({
      status: 400,
      error: 'Bad data'
    });
  } else {
    const competence = new Competence(competenceToSave);
    console.log(competence);
    competence.save(function(err, competenceSaved) {
      console.log(err);
      if (err || competenceSaved === null) {
        res.status(500);
        return res.json({
          status: 404,
          error: 'Error saving the competence'
        });
      }
      const competenceToSend = competenceDto.serviceToDto(competenceSaved);
      return res.json(competenceToSend);
    });
  }
});


// Get competence by id
router.get('/competence/:id', function(req, res, next) {
  Competence.findById(mongojs.ObjectId(req.params.id), function(err, competenceSaved) {
    if (err) {
      res.statusCode = 500;
      return res.send(err);
    }
    const competenceToSend = competenceDto.serviceToDto(competenceSaved);
    res.statusCode = 200;
    return res.json(competenceToSend);
  });
});


// Delete one user by ID
router.delete('/users/:id/delete', function(req, res, next) {
  User.findOneAndRemove({
    _id: mongojs.ObjectId(req.params.id)
  }, function(err, user) {
    if (err) {
      return res.send(err);
    }
    const userToDelete = userDto.serviceToDto(user);
    return res.json(userToDelete);
  });
});

// Update user
router.put('/user/:id/update', function(req, res, next) {
  var user = req.body;
  User.update({
    _id: mongojs.ObjectId(req.params.id)
  }, user, {}, function(err, userUpdated) {
    if (err) {
      return res.send(err);
    }
    const userToUpdate = userDto.serviceToDto(user);
    return res.json(userToUpdate);
  });
});

module.exports = router;
