function serviceToDto(serviceData) {
  const competenceDto = {
    id: '',
    name: '',
    description: '',
    state: '',
    percentageDone: '',
    idUser: '',
    idCoach: '',
    startDate: '',
    finishDate: '',
    createdAt: ''
  };
  competenceDto.id = serviceData._id;
  competenceDto.name = serviceData.name;
  competenceDto.description = serviceData.description;
  competenceDto.state = serviceData.state;
  competenceDto.percentageDone  = serviceData.percentageDone;
  competenceDto.idUser = serviceData.idUser;
  competenceDto.idCoach = serviceData.idCoach;
  competenceDto.startDate = serviceData.startDate;
  competenceDto.finishDate = serviceData.finishDate;
  competenceDto.createdAt = serviceData.createdAt;
    console.log('dtoooo', competenceDto);
  return competenceDto;
}


function serviceListToDto(serviceDataList) {
  const competenceDtoList = [];
  for (let i = 0; i < serviceDataList.length; i++) {
    const competenceDto = {
      id: '',
      name: '',
      description: '',
      state: '',
      percentageDone: '',
      idUser: '',
      idCoach: '',
      startDate: '',
      finishDate: '',
      createdAt: ''
    };

    competenceDto.id = serviceDataList[i]._id;
    competenceDto.name = serviceDataList[i].name;
    competenceDto.description = serviceDataList[i].description;
    competenceDto.state = serviceDataList[i].state;
    competenceDto.percentageDone  = serviceDataList[i].percentageDone;
    competenceDto.idUser = serviceDataList[i].idUser;
    competenceDto.idCoach = serviceDataList[i].idCoach;
    competenceDto.startDate = serviceDataList[i].startDate;
    competenceDto.finishDate = serviceDataList[i].finishDate;
    competenceDto.createdAt = serviceDataList[i].createdAt;

    competenceDtoList.push(competenceDto);
  }

  return competenceDtoList;
}

exports.serviceListToDto = serviceListToDto;
exports.serviceToDto = serviceToDto;

// module.exports = serviceToDto;
