// grab the things we need
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// create a schema
const competenceSchema = new Schema({
  name: String,
  description: String,
  state: String,
  percentageDone: String,
  idUser: String,
  idCoach: String,
  startDate: String,
  finishDate: String,
  createdAt: String
});

// the schema is useless so far
// we need to create a model using it
const Competence = mongoose.model('Competence', competenceSchema);

// make this available to our users in our Node applications
module.exports = Competence;
