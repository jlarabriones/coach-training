function serviceToDto(serviceData) {
  const fileDto = {
    id: '',
    name: '',
    description: '',
    state: '',
    idCompetence: '',
    idUser: '',
    idCoach: '',
    idRoutine: '',
    startDate: '',
    finishDate: '',
    createdAt: '',
    actionPlans: []
  };

  fileDto.id = serviceData._id;
  fileDto.name = serviceData.name;
  fileDto.description = serviceData.description;
  fileDto.state = serviceData.state;
  fileDto.percentageDone  = serviceData.percentageDone;
  fileDto.idCompetence = serviceData.idCompetence;
  fileDto.idUser = serviceData.idUser;
  fileDto.idCoach = serviceData.idCoach;
  fileDto.idRoutine = serviceData.idRoutine;
  fileDto.startDate = serviceData.startDate;
  fileDto.finishDate = serviceData.finishDate;
  fileDto.createdAt = serviceData.createdAt;
  for (var i = 0; i < serviceData.actionPlans.length; i++) {
    const actionPlan = {
      id: '',
      description: '',
      state: ''
    };
    actionPlan.id = serviceData.actionPlans[i]._id;
    actionPlan.description = serviceData.actionPlans[i].description;
    actionPlan.state = serviceData.actionPlans[i].state;

    fileDto.actionPlan.push(actionPlan);
  }
  return fileDto;
}

function serviceListToDto(serviceDataList) {
  const fileDtoList = [];
  for (let i = 0; i < serviceDataList.length; i++) {
    const fileDto = {
      id: '',
      name: '',
      description: '',
      state: '',
      idCompetence: '',
      idUser: '',
      idCoach: '',
      idRoutine: '',
      startDate: '',
      finishDate: '',
      createdAt: '',
      actionPlans: []
    };
    const actionPlan = {
      id: '',
      description: '',
      state: ''
    };

    fileDto.id = serviceDataList[i]._id;
    fileDto.name = serviceDataList[i].name;
    fileDto.description = serviceDataList[i].description;
    fileDto.state = serviceDataList[i].state;
    fileDto.idCompetence = serviceDataList[i].idCompetence;
    fileDto.idUser = serviceDataList[i].idUser;
    fileDto.idCoach = serviceDataList[i].idCoach;
    fileDto.idRoutine = serviceDataList[i].idRoutine;
    fileDto.startDate = serviceDataList[i].startDate;
    fileDto.finishDate = serviceDataList[i].finishDate;
    fileDto.createdAt = serviceDataList[i].createdAt;

    for (var a = 0; a < serviceDataList[i].actionPlans.length; a++) {
      const actionPlan = {
        id: '',
        description: '',
        state: ''
      };
      actionPlan.id = serviceDataList[i].actionPlans[a]._id;
      actionPlan.description = serviceDataList[i].actionPlans[a].description;
      actionPlan.state = serviceDataList[i].actionPlans[a].state;

      fileDto.actionPlan.push(actionPlan);
    }
    fileDtoList.push(fileDto);
  }
  return fileDtoList;
}

exports.serviceListToDto = serviceListToDto;
exports.serviceToDto = serviceToDto;

// module.exports = serviceToDto;
