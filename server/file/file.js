const express = require('express');
const mongojs = require('mongojs');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const router = express.Router();
const config = require('../config');
// const db = mongojs('mongodb://jorge:jorge@ds155811.mlab.com:55811/routines', ['user']);
const File = require('./file.schema');
const fileDto = require('./file.dto');

// Get all the files by competence and user
router.get('/competence/:idCompetence/files/:idUser', function(req, res, next) {
  File.find({
    "idCompetence": mongojs.ObjectId(req.params.idCompetence),
    "idUser":  mongojs.ObjectId(req.params.idUser)
  }, function(err, files) {
    if (err) {
      return res.send(err);
    }
    const fileToSend = fileDto.serviceListToDto(files);
    res.statusCode = 200;
    return res.json(fileToSend);
  });
});

// Get file by Id
router.get('/file/:id', function(req, res, next) {
  File.findById(mongojs.ObjectId(req.params.id), function(err, file) {
    if (err) {
      return res.send(err);
    }
    const fileToSend = fileDto.serviceToDto(file);
    res.statusCode = 200;
    return res.json(fileToSend);
  });
});

// Create  new file
router.post('/file/routine/:id/create', function(req, res, next) {
  var fileToSave = req.body;
  fileToSave.createdAt = new Date();
  if (!fileToSave.name) {
    res.status(400);
    return res.json({
      status: 400,
      error: 'Bad data'
    });
  } else {
    const file = new File(fileToSave);
    console.log(file);
    file.save(function(err, fileSaved) {
      console.log(err);
      if (err || fileSaved === null) {
        res.status(500);
        return res.json({
          status: 404,
          error: 'Error saving the file'
        });
      }
      const fileToSend = fileDto.serviceToDto(fileSaved);
      return res.json(fileToSend);
    });
  }
});


module.exports = router;
