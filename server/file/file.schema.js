// grab the things we need
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// create a schema
const fileSchema = new Schema({
  name: String,
  description: String,
  state: String,
  idCompetence: String,
  idUser: String,
  idCoach: String,
  idRoutine: String,
  startDate: String,
  finishDate: String,
  createdAt: String,
  actionPlans: []
});

// the schema is useless so far
// we need to create a model using it
const File = mongoose.model('File', fileSchema);

// make this available to our users in our Node applications
module.exports = File;
